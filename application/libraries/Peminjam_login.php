<?php

defined('BASEPATH') or exit('No direct script access allowed');

class peminjam_login
{
	protected $ci;

	public function __construct()
	{
		$this->ci = &get_instance();
		$this->ci->load->model('m_auth');
	}

	public function login($email, $password)
	{
		$cek = $this->ci->m_auth->login_peminjam($email, $password);
		if ($cek) {
			$id_peminjam = $cek->id_peminjam;
			$nama_peminjam = $cek->nama_peminjam;
			$email = $cek->email;
			$foto = $cek->foto;
			//buat session
			$this->ci->session->set_userdata('id_peminjam', $id_peminjam);
			$this->ci->session->set_userdata('nama_peminjam', $nama_peminjam);
			$this->ci->session->set_userdata('email', $email);
			$this->ci->session->set_userdata('foto', $foto);
			redirect('home');
		} else {
			//jika salah
			$this->ci->session->set_flashdata('error', 'Email Atau Password Salah');
			redirect('peminjam/login');
		}
	}

	public function proteksi_halaman()
	{
		if ($this->ci->session->userdata('nama_peminjam') == '') {
			$this->ci->session->set_flashdata('error', 'Anda Belum Login');
			redirect('peminjam/login');
		}
	}

	public function logout()
	{
		$this->ci->session->unset_userdata('id_peminjam');
		$this->ci->session->unset_userdata('nama_peminjam');
		$this->ci->session->unset_userdata('email');
		$this->ci->session->unset_userdata('foto');
		$this->ci->session->set_flashdata('pesan', 'Anda Berhasil Logout');
		redirect('peminjam/login');
	}
}