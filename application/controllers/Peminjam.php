<?php

defined('BASEPATH') or exit('No direct script access allowed');

class peminjam extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_peminjam');
		$this->load->model('m_auth');
	}


	public function register()
	{
		$this->form_validation->set_rules('nama_peminjam', 'nama_peminjam', 'required', array(
			'required' => '%s Harus Diisi'
		));
		$this->form_validation->set_rules('email', 'E-mail', 'required|is_unique[tbl_peminjam.email]', array(
			'required' => '%s Harus Diisi',
			'is_unique' => '%s Email Sudah Ini Terdaftar'
		));
		$this->form_validation->set_rules('password', 'password', 'required', array(
			'required' => '%s Harus Diisi'
		));
		$this->form_validation->set_rules('ulangi_password', 'Ulangi Password', 'required|matches[password]', array(
			'required' => '%s Harus Diisi',
			'matches' => '%s Password Tidak Sama'
		));


		if ($this->form_validation->run() == FALSE) {
			$data = array(
				'title' => 'Register peminjam',
				'isi' => 'v_register',
			);
			$this->load->view('layout/v_wrapper_frontend', $data, FALSE);
		} else {
			$data = array(
				'nama_peminjam' => $this->input->post('nama_peminjam'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
			);
			$this->m_peminjam->register($data);
			$this->session->set_flashdata('pesan', 'Register Berhasil Silahkan Login Kembali');
			redirect('peminjam/register');
		}
	}

	public function login()
	{
		$this->form_validation->set_rules('email', 'E-Mail', 'required', array(
			'required' => '%s Harus Diisi'
		));
		$this->form_validation->set_rules('password', 'Password', 'required', array(
			'required' => '%s Harus Diisi'
		));


		if ($this->form_validation->run() == TRUE) {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$this->peminjam_login->login($email, $password);
		}
		$data = array(
			'title' => 'Login peminjam',
			'isi' => 'v_login_peminjam',
		);
		$this->load->view('layout/v_wrapper_frontend', $data, FALSE);
	}

	public function logout()
	{
		$this->peminjam_login->logout();
	}

	public function akun()
	{
		//proteksi halaman
		$this->peminjam_login->proteksi_halaman();
		$data = array(
			'title' => 'Akun Saya',
			'isi' => 'v_akun_saya',
		);
		$this->load->view('layout/v_wrapper_frontend', $data, FALSE);
	}
}