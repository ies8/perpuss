<!-- Default box -->
<div class="card card-solid">
	<div class="card-body">
		<div class="row">
			<div class="col-12 col-sm-6">
				<h3 class="d-inline-block d-sm-none"><?= $buku->nama_buku ?></h3>
				<div class="col-12">
					<img src="<?= base_url('assets/gambar/' . $buku->gambar) ?>" class="product-image" alt="Product Image">
				</div>
				<div class="col-12 product-image-thumbs">
					<div class="product-image-thumb active"><img src="<?= base_url('assets/gambar/' . $buku->gambar) ?>" alt="Product Image"></div>
					<?php foreach ($gambar as $key => $value) { ?>
						<div class="product-image-thumb"><img src="<?= base_url('assets/gambarbuku/' . $value->gambar) ?>" alt="Product Image"></div>
					<?php } ?>

				</div>
			</div>
			<div class="col-12 col-sm-6">
				<h3 class="my-3"><?= $buku->nama_buku ?></h3>
				<hr>
				<h5><?= $buku->nama_kategori ?></h5>
				<hr>
				<p><?= $buku->deskripsi ?></p>
				<hr>

				<div class="bg-gray py-2 px-3 mt-4">
					<h2 class="mb-0">
						Rp. <?= number_format($buku->harga, 0) ?>.00
					</h2>
				</div>
				<hr>
				<?php
				echo form_open('belanja/add');
				echo form_hidden('id', $buku->id_buku);
				echo form_hidden('price', $buku->harga);
				echo form_hidden('name', $buku->nama_buku);
				echo form_hidden('redirect_page', str_replace('index.php/', '', current_url()));
				?>
				<div class="mt-4">
					<div class="row">
						<div class="col-sm-2">
							<input type="number" name="qty" class="form-control" value="1" min="1">
						</div>
						<div class=" col-sm-8">
							<button type="submit" class="btn btn-primary btn-flat swalDefaultSuccess">
								<i class=""></i>
								Read
							</button>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>

				

			</div>
		</div>

	</div>
	<!-- /.card-body -->
</div>
<!-- /.card -->
<!-- jQuery -->

<script src="<?= base_url() ?>template/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?= base_url() ?>template/dist/js/demo.js"></script>
<script type="text/javascript">
	$(function() {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000
		});

		$('.swalDefaultSuccess').click(function() {
			Toast.fire({
				icon: 'success',
				title: 'Buku dimasukkan ke keranjang'
			})
		});
	});
</script>
