<div class="invoice p-3 mb-3">
	<!-- title row -->
	<div class="row">
		<div class="col-12">			
		</div>

	</div>

	<div class="row">

		<div class="col-12 table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Qty</th>
						<th width="150px" class="text-center">Harga</th>
						<th>buku</th>
						<th class="text-center">Total Harga</th>
						<th class="text-center">Jumlah Halaman</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1;
					$tot_Berat = 0;
					foreach ($this->cart->contents() as $items) {
						$buku = $this->m_home->detail_buku($items['id']);
						$Berat = $items['qty'] * $buku->berat;

						$tot_Berat = $tot_Berat + $Berat;
					?>
						<tr>
							<td><?php echo $items['qty']; ?></td>
							<td class="text-center">Rp. <?php echo number_format($items['price'], 0); ?></td>
							<td><?php echo $items['name']; ?></td>
							<td class="text-center">Rp. <?php echo  number_format($items['subtotal'], 0); ?></td>
							<td class="text-center"><?= $Berat  ?> Hal</td>
						</tr>
					<?php } ?>

				</tbody>
			</table>
		</div>
		
	</div>
	<!-- /.row -->
	<?php
	echo validation_errors('<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
	?>
	<?php
	echo form_open('belanja/cekout');
	$no_order = date('Ymd') . strtoupper(random_string('alnum', 8));
	?>
	<div class="row">
		<!-- accepted payments column -->
		<div class="col-sm-8 invoice-col">			
			</div>
		</div>
		<!-- /.col -->
		<div class="col-4">
			<div class="table-responsive">
				<table class="table">
					<tr>
						<th style="width:50%">Grand Total:</th>
						<th>Rp. <?php echo number_format($this->cart->total(), 0); ?></th>
					</tr>
				</table>
			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->

	<!-- awal Simpan Transaksi -->
	<input name="no_order" value="<?= $no_order ?>" hidden>
	<input name="estimasi" hidden>
	<input name="ongkir" hidden>
	<input name="Berat" value="<?= $tot_Berat ?>" hidden><br>
	<input name="grand_total" value="<?= $this->cart->total() ?>" hidden>
	<input name="total_bayar" hidden>
	<!-- end Simpan Transaksi -->
	<!-- awal Simpan Rincian Transaksi -->
	<?php
	$i = 1;
	foreach ($this->cart->contents() as $items) {
		echo form_hidden('qty' . $i++, $items['qty']);
	}

	?>
	<!-- end Simpan Rinci Transaksi -->
	<div class="row no-print">
		<div class="col-12">
			<a href="<?= base_url('belanja')  ?>" class="btn btn-warning"><i class="fas fa-backward"></i> Kembali Ke Keranjang</a>
			<button type="submit" class="btn btn-primary float-right" style="margin-right: 5px;">
				<i class="fas fa-shopping-cart"></i> Checkout
			</button>
		</div>
	</div>
	<?php echo form_close() ?>
</div>